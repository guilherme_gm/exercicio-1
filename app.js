const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const port = 8000;
const token = 777;

app.use(bodyParser.urlencoded({extended:true}));
app.use (bodyParser.json());

app.listen(port, () => console.log(`Servidor executando na porta ${port}`));

app.get('/clientes', (req, res) => {
    let src = req.query;
    res.send(`Nome do Cliente: ${src.name}`);
});

app.post('/clientes', (req, res) => {
    let data = req.body;
    
    let header_ = req.headers['access'];
    console.log("Valor Access: " + header_);

    if (header_ == token) {
        res.send(`Nome: ${data.name} - Sobrenome: ${data.lastName} - Idade: ${data.age}`);
    } else {
        res.send(`Accesso negado!`);
    }
});


app.get('/funcionarios', (req, res) => {
    let src = req.query;
    res.send(`Nome do Funcionario: ${src.name}`);
});

app.delete('/funcionarios/:id', (req, res) => {
    let header_ = req.headers['access'];
    console.log("Valor Access: " + header_);

    let id = req.params.id;

    if (header_ == token) {
        res.send(`Funcionario Excluido - ID ${id}`);
    } else {
        res.send(`Accesso negado!`);
    }
});

app.put('/funcionarios', (req, res) => {
    let header_ = req.headers['access'];
    console.log("Valor Access: " + header_);

    let data = req.body;

    if (header_ == token) {
        res.send(`Funcionario Atualizado - ID ${data.id}
                Nome: ${data.name}
                Sobrenome: ${data.lastName}
                Idade: ${data.age}`);
    } else {
        res.send(`Accesso negado!`);
    }
});